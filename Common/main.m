//
//  main.m
//  CICD Demo
//
//  Created by Amr Muhammad on 01/06/2023.
//

#import <UIKit/UIKit.h>

#if BM
#import "BM-Swift.h"
#elif CA
#import "CA-Swift.h"
#else
#import "NBE-Swift.h"
#endif


int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        NSString *delegateClass;
#if CA
        delegateClass = NSStringFromClass([CAAppDelegate class]);
#elif BM
        delegateClass = NSStringFromClass([BMAppDelegate class]);
#elif NBE
        delegateClass = NSStringFromClass([NBEAppDelegate class]);
#else
        delegateClass = NSStringFromClass([AppDelegate class]);
#endif

        int retVal = -1;
        @try {
            retVal = UIApplicationMain(argc, argv, nil,delegateClass);
        }
        @catch (NSException* exception) {
        #if DEBUG
            NSLog(@"Uncaught exception: %@", exception.description);
            NSLog(@"Stack trace: %@", [exception callStackSymbols]);
        #endif
        }
        
        return retVal;
    }
}
