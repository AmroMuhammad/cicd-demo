//
//  ViewController.swift
//  BM
//
//  Created by Amr Muhammad on 01/06/2023.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var AppNameLabel: UILabel!
    @IBOutlet weak var buildNumberLabel: UILabel!
    @IBOutlet weak var versionNumberLabel: UILabel!
    @IBOutlet weak var bundleIDLabel: UILabel!
    @IBOutlet weak var googleAPILabel: UILabel!
    @IBOutlet weak var environmentKeyLabel: UILabel!
    @IBOutlet weak var baseURLLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.testAppDelegateFunc()
        
        AppNameLabel.text = ConfigManager.getValueFor(key: .appName)
        buildNumberLabel.text = ConfigManager.getValueFor(key: .buildNumber)
        versionNumberLabel.text = ConfigManager.getValueFor(key: .versionNumber)
        bundleIDLabel.text = ConfigManager.getValueFor(key: .appBundleID)
        googleAPILabel.text = ConfigManager.getValueFor(key: .googleAPIKey)
        environmentKeyLabel.text = ConfigManager.getValueFor(key: .environmentKey)
        baseURLLabel.text = ConfigManager.getValueFor(key: .baseURL)
    }


}

