//
//  ConfigManager.swift
//  CICD Demo
//
//  Created by Amr Muhammad on 02/06/2023.
//

import Foundation

final class ConfigManager{
    enum configKeys : String{
        case buildNumber = "CFBundleVersion"
        case versionNumber = "CFBundleShortVersionString"
        case environmentKey = "ENVIRONMENT_KEY"
        case appName = "CFBundleName"
        case baseURL = "BASE_URL"
        case googleAPIKey = "GOOGLE_API_KEY"
        case appBundleID = "CFBundleIdentifier"
    }
    
    static func getValueFor(key:configKeys)->String{
        let value = Bundle.main.object(forInfoDictionaryKey: key.rawValue) as? String ?? "N/A"
        return value
    }
}
